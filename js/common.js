'use strict';

const c = document.createElement.bind(document),
    id = document.getElementById.bind(document),
    q = document.querySelector.bind(document),
    qs = document.querySelectorAll.bind(document),
    wait  = ms => new Promise(resolve => setTimeout(() => resolve(), ms)),
    frame = cb => new Promise(resolve => requestAnimationFrame(() => {
            if (cb) cb();
            resolve();
        })),
    findTarget = (node, type) => {
        if (!node || !node.tagName) return;
        else if (node.tagName.toLowerCase() == type.toLowerCase()) return node;
        else return findTarget(node.parentNode, type);
    },
    callbacks = {},
    port = chrome.runtime.connect({name:"port-from-cs"});

const sendMessage = (type, data, callbackFn) => {
    let messageId = Math.round(Math.random() * 4200000000); // do you feel lucky
  
    if (callbackFn) callbacks[messageId] = {fn: callbackFn};
    
    port.postMessage({...data, type: type, id: messageId});
}

port.onMessage.addListener(message => {
    if (message.id && typeof callbacks[message.id] !== "undefined") {
        callbacks[message.id].fn(message);
        delete callbacks[message.id];
    }
});