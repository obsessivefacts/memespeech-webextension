'use strict';

// store references to html elements from the view
const divFlexLayout         = id('div-flex-layout'),
    inputEnableEncryption   = id('input-enable-encryption'),
    divEncryptionFields     = id('div-encryption-fields'),
    textareaCarrier         = id('textarea-carrier'),
    textareaCreateResults   = id('textarea-create-results'),
    divOpenNewTab           = id('div-open-new-tab'),
    divNotificationArea     = id('div-notification-area'),
    spanNotification        = id('span-notification'),
    inputPlaintext          = id('input-plaintext'),
    inputPassword           = id('input-password'),
    aChooseSavedText        = id('a-choose-saved-text'),
    buttonCreateSubmit      = id('button-create-submit'),
    buttonCreateReset       = id('button-create-reset'),
    buttonCreateRegenerate  = id('button-create-regenerate'),
    divCreateResultsBack    = id('div-create-results-back'),
    liNavCreate             = id('li-nav-create'),
    liNavDecrypt            = id('li-nav-decrypt'),
    inputDecryptionPassword = id('input-decryption-password'),
    buttonDecryptSubmit     = id('button-decrypt-submit'),
    textareaMemespeech      = id('textarea-memespeech'),
    divDecryptError         = id('div-decrypt-error'),
    textareaDecrypted       = id('textarea-decrypted'),
    buttonDecryptClose      = id('button-decrypt-close'),
    divDecryptResultsBack   = id('div-decrypt-results-back'),
    divModalTooltip         = id('div-modal-tooltip'),
    imgModalTooltip         = id('img-modal-tooltip'),
    h3ModalTooltip          = id('h3-modal-tooltip'),
    pModalTooltip           = id('p-modal-tooltip'),
    buttonModalTooltipClose = id('button-modal-tooltip-close'),
    spanCopyNotice          = id('span-copy-notice'),
    svgIconCopy             = id('icon-copy'),
    divSavedTextPopup       = id('div-saved-text-popup'),
    ulSavedTextPopupItems   = id('ul-saved-text-popup-items'),
    aManageSavedText        = id('a-manage-saved-text'),
    divCreateManageBack     = id('div-create-manage-back'),
    divCreateManageAdd      = id('div-create-manage-add'),
    divCreateManageEditBack = id('div-create-manage-edit-back'),
    buttonCreateManageDone  = id('button-create-manage-done'),
    tbodyCreateManage       = id('tbody-create-manage'),
    h1AddOrEditSavedText    = id('h1-create-manage-add-or-edit'),
    buttonSubmitSavedText   = id('button-submit-saved-text'),
    inputSavedTextTitle     = id('input-saved-text-title'),
    textareaSavedText       = id('textarea-saved-text'),
    inputHiddenSavedTextId  = id('input-hidden-saved-text-id'),
    divsUiView              = qs('.content'),
    svgLogo                 = id('svg-logo'),
    divCreditsPopup         = id('div-credits-popup'),
    buttonCreditsDismiss    = id('button-credits-dismiss'),
    inputToggleConfig       = id('input-toggle-config'),
    FULLSCREEN              = window.location.hash === '#FULLSCREEN';

// initialize "global" variables
let loading = false,
    memespeech,
    config = {},
    curUiView = 'create',
    textareaUpdateDebounce,
    backgroundSyncDebounce,
    byteBudgets,
    i18n,
    creditsLastAnimated,
    tooltips = {
        carrierText: {
            image: '/images/brainlet-joe.png',
            heading: null,
            description: null
        },
        encryption: {
            image: '/images/brainlet-helmet.png',
            heading: null,
            description: null
        },
        memespeechText: {
            image: '/images/brainlet-pointdexter.png',
            heading: null,
            description: null
        },
        decryptPassphrase: {
            image: '/images/brainlet-microwave.png',
            heading: null,
            description: null
        }
    };

// show or hide the encryption fields depending on the toggle switch state
const handleInputEnableEncryption = e => {
    if (inputEnableEncryption.checked)
        divEncryptionFields.classList.remove('hidden');
    else
        divEncryptionFields.classList.add('hidden');

    handleTextareaCarrierChange();

    // sync state with the background script if this came from a user action
    if (e) syncBackgroundUiStateDebounce();
};

// update the form state based on the Carrier Text textarea value
const handleTextareaCarrierChange = e => {
    const _updateCarrierText = () => {
        handleResetButtonState();

        if (!textareaCarrier.value) {
            setNotification(i18n.createNotificationDefault, 'info');
            buttonCreateSubmit.disabled = true;
            byteBudgets = null;
            return false;
        }

        memespeech.setCarrierText(textareaCarrier.value);
        byteBudgets = memespeech.computeAESByteBudget();

        if (byteBudgets.byteBudgetCompact <= 0) {
            setNotification(i18n.createNotificationTooShort, 'warn');
            inputPlaintext.disabled = true;
            inputPassword.disabled = true;
            buttonCreateSubmit.disabled = inputEnableEncryption.checked;
            return false;
        }

        inputPlaintext.disabled = false;
        inputPassword.disabled = false;

        handleByteBudget();
    };
    if (textareaUpdateDebounce) clearTimeout(textareaUpdateDebounce);
    textareaUpdateDebounce = setTimeout(_updateCarrierText, 50);

    // sync state with the background script if this came from a user action
    if (e) syncBackgroundUiStateDebounce();
};

// update the notification that displays the bytes available for encryption
const handleByteBudget = e => {
    if (!byteBudgets || byteBudgets.byteBudgetCompact <= 0) return;

    buttonCreateSubmit.disabled = false;

    let secretTxt = inputPlaintext.value,
        secretTxtLength = Memespeech.DataUtils.stringToBytes(secretTxt).length,
        budget = byteBudgets.byteBudgetCompact - secretTxtLength;

    let formattedBudget = Math.abs(budget),
        formattedLabel = formattedBudget == 1 ? i18n.byteSingular : i18n.bytePlural;

    if (formattedBudget >= 1000) {
        formattedBudget = Math.round(formattedBudget / 1000);
        formattedLabel = formattedBudget == 1 ? i18n.kilobyteSingular : i18n.kilobytePlural;
    }

    if (budget > 0) {
        setNotification(
            secretTxtLength ? i18n.createNotificationRemaining : i18n.createNotificationOK,
            'ok',
            {
                budgetNum: formattedBudget,
                budgetLabel: formattedLabel
            }
        );
    } else if (budget == 0) {
        setNotification(i18n.createNotificationMaxed, 'warn');
    } else {
        if (secretTxtLength && inputEnableEncryption.checked)
            buttonCreateSubmit.disabled = true;

        setNotification(i18n.createNotificationSecretTextOverflow, 'error', {
            budgetNum: formattedBudget,
            budgetLabel: formattedLabel
        });
    }

    handleResetButtonState();

    // sync state with the background script if this came from a user action
    if (e) syncBackgroundUiStateDebounce();
};

// decide whether to enable or disable the reset button
const handleResetButtonState = () => {
    if (textareaCarrier.value.length > 0
        || inputPlaintext.value.length > 0
        || inputPassword.value.length > 0
    ) {
        buttonCreateReset.disabled = false;
    } else {
        buttonCreateReset.disabled = true;
    }
};

// decide whether to enable or disable the decrypt button
const handleDecryptButtonState = (e) => {
    buttonDecryptSubmit.disabled = textareaMemespeech.value.length == 0;

    // sync state with the background script if this came from a user action
    if (e) syncBackgroundUiStateDebounce();
};

// set the notification area, offering optional substitutions from our locale
const setNotification = (text, status, substitute) => {
    divNotificationArea.classList.remove('info', 'error', 'warn', 'ok');
    divNotificationArea.classList.add(status);
    
    if (substitute) {
        for (var key in substitute) {
            if (!substitute.hasOwnProperty(key)) continue;

            text = text.replace('{{' + key + '}}', substitute[key]);
        }
    }
    spanNotification.textContent = text;
};

// watch for [Enter] key in the password fields and do the submit when detected.
const checkCreatePasswordEnterKey = e => e.keyCode === 13 ?
        handleCreateSubmit() : syncBackgroundUiStateDebounce(),
      checkDecryptPasswordEnterKey = e => e.keyCode === 13 ?
        handleDecryptSubmit() : syncBackgroundUiStateDebounce();

// handle the submit button for the Create view
const handleCreateSubmit = async function() {
    if (buttonCreateSubmit.disabled) return false;

    applyLoadingState('create', true);

    // helper function to bail out and display a message if an error occurs
    const _raiseError = function(userMsg, debugMsg) {
        applyLoadingState('create', false);
        console.error(debugMsg);
        setNotification(userMsg, 'error');
        divNotificationArea.classList.add('nudge');
        setTimeout(() => divNotificationArea.classList.remove('nudge'), 20);
    }

    let memespeechText,
        carrierText = textareaCarrier.value,
        plaintext = inputPlaintext.value,
        password = inputPassword.value,
        enableEncryption = inputEnableEncryption.checked
                           && carrierText.length > 0
                           && plaintext.length > 0;

    // reset our carrier text into the memespeech instance, just to be safe.
    memespeech.setCarrierText(carrierText);
    
    if (enableEncryption) {
        // select the strongest encoding mode available for this Carrier Text
        let budgets = memespeech.computeAESByteBudget(),
            plaintextLength = Memespeech.DataUtils.stringToBytes(plaintext).length,
            mode = 'compact';

        // if we have extra budget we'll use 'standard' mode for moar security
        if (budgets.byteBudgetStandard - plaintextLength >= 0)
            mode = 'standard'

        // intialize the AES cipher with the password and our encoding mode
        try {
            await memespeech.initCipherFromPassphrase(password, mode);
        } catch(err) {
            return _raiseError(i18n.createErrorPasswordIsRequired, err);
        }

        // use the cipher to encrypt the plaintext into a Cryptographic Payload
        await memespeech.encrypt(plaintext);

        // format the Cryptographic Payload back into the Carrier Text
        memespeechText = await memespeech.toString();
    } else {
        // NOT ENCRYPTING! Encode a decoy Crypto Payload into the Carrier Text
        memespeechText = await memespeech.generateDecoyText();
    }
    textareaCreateResults.value = memespeechText;

    // done generating, switch to the Create Results UI View
    await frame(() => {
        applyLoadingState('create', false);
        showFloatingView('create');
    })
};

// reset the fields in the Create view
const handleCreateReset = async function() {
    if (buttonCreateReset.disabled) return false;

    textareaCarrier.value = '';
    inputPlaintext.value = '';
    inputPassword.value = '';
    inputEnableEncryption.checked = false;

    resetMemespeech();
    handleInputEnableEncryption();
    syncBackgroundUiStateDebounce();
};

// handle the regenerate button, allowing the user to re-scramble the Memespeech
const handleCreateRegenerate = async function() {
    applyLoadingState('create-results', true);
    memespeech.generateRandomSalt();
    await handleCreateSubmit();
    applyLoadingState('create-results', false);
};

// Copies the Memespeech text to clipboard, shows an acknowledgement animation
const copyMemespeechToClipboard = async function() {
    textareaCreateResults.select();
    document.execCommand('copy');
    textareaCreateResults.setSelectionRange(0, 0);
    textareaCreateResults.blur();
    if (spanCopyNotice.classList.contains('animate')) return;
    spanCopyNotice.classList.add('animate');
    await wait(2000);
    spanCopyNotice.classList.remove('animate');
};

// handles the decryption of memespeech text
const handleDecryptSubmit = async function() {
    if (buttonDecryptSubmit.disabled) return false;

    applyLoadingState('decrypt', true);

    // helper function to bail out and display a message if an error occurs
    const _raiseError = function(userMsg, debugMsg) {
        applyLoadingState('decrypt', false);
        console.error(debugMsg);
        divDecryptError.querySelector('span').textContent = userMsg;
        divDecryptError.classList.add('nudge');
        setTimeout(() => divDecryptError.classList.remove('hidden'), 20);
        setTimeout(() => divDecryptError.classList.remove('nudge'), 40);
    }

    let _ms = new Memespeech(), plaintext;

    // try to parse a Memespeech Cryptographic Payload out of the given text
    try {
        await _ms.parse(textareaMemespeech.value);
    } catch(err) {
        return _raiseError(i18n.decryptErrorCannotParse, err);
    }

    // try to initialize the cipher from the passphrase
    try {
        await _ms.initCipherFromPassphrase(inputDecryptionPassword.value);
    } catch(err) {
        return _raiseError(i18n.decryptErrorMissingPassphrase, err);
    }

    // try to decrypt using the key generated from the passphrase
    try {
        plaintext = await _ms.decrypt();
    } catch(err) {
        return _raiseError(i18n.decryptFail, err);
    }

    // decryption worked!
    textareaDecrypted.value = plaintext;
    divDecryptError.classList.add('hidden');
    
    // show the success animation if the text is not too long
    if (textareaMemespeech.value.length < 10000) 
        await decryptionSuccessAnimation();

    await frame(() => showFloatingView('decrypt'));

    await wait(500);

    applyLoadingState('decrypt', false);
    textareaMemespeech.value = '';
    inputDecryptionPassword.value = '';
    handleDecryptButtonState();
};

// visual flair for when decryption succeeds: fade the Memespeech text to binary
const decryptionSuccessAnimation = async function() {
    const _shuffle = array => {
            for (let i = array.length - 1; i > 0; i--) {
                const j = Math.floor(Math.random() * (i + 1));
                [array[i], array[j]] = [array[j], array[i]];
            }
        },
        _replaceAt = (index, replacement) => {
            let val = textareaMemespeech.value;
            textareaMemespeech.value = val.substr(0, index)
                + replacement
                + val.substr(index + replacement.length);
        },
        matches = [...textareaMemespeech.value.matchAll(/[A-Za-z]/g)],
        charArray = matches.map(m => { return {str: m[0], index: m.index} });

    _shuffle(charArray);

    let i = 0, replacementsPerFrame = Math.ceil(charArray.length / 120);

    while (charArray.length) {
        if (i++ % replacementsPerFrame == 0) await frame();

        let char = charArray.pop();

        _replaceAt(char.index, char.str === char.str.toUpperCase() ? "1" : "0");
    }

    await wait(250);
};

// handle the Close and Back button actions in the Decrypt Results UI view
const handleDecryptResultsClose = justGoBack => {
    if (FULLSCREEN || justGoBack) {
        hideFloatingView('decrypt')
        setTimeout(() => textareaDecrypted.value = '', 500);
    }
    else window.close();
};

// apply the loading state to the given UI View
const applyLoadingState = (uiView, isLoading) => {
    loading = isLoading ? true : false;

    let el = q('.content.' + uiView),
        submitButton = el.querySelector('button.submit'),
        resetButton = el.querySelector('button.reset');

    if (isLoading)
        el.classList.add('loading');
    else
        el.classList.remove('loading');

    submitButton.disabled = loading;
    if (resetButton) resetButton.disabled = loading;
};

// show the floating UI View related to the current page action
const showFloatingView = uiView => {
    let dashIdx = uiView.lastIndexOf('-'),
        primary = dashIdx === -1 ? uiView : uiView.substr(0, dashIdx),
        secondary = dashIdx === -1 ? 'results' : uiView.substr(dashIdx + 1),
        mainEl = q('.content.' + primary),
        floatingEl = q('.content.' + primary + '-' + secondary);

    mainEl.classList.add('background');
    if (mainEl.classList.contains('floating')) {
        mainEl.classList.remove('foreground');
    }
    floatingEl.classList.remove('hidden');

    setTimeout(() => floatingEl.classList.add('foreground'), 20);
    setTimeout(() => {
        mainEl.classList.add('hidden')
        floatingEl.classList.add('active');

        curUiView = primary + '-' + secondary;
    }, 500);
    syncBackgroundUiStateDebounce();
};

// hide the floating UI View related to the current page action
const hideFloatingView = uiView => {
    let dashIdx = uiView.lastIndexOf('-'),
        backPath = dashIdx === -1 ? uiView : uiView.substr(0, dashIdx),
        floating = dashIdx === -1 ? 'results' : uiView.substr(dashIdx + 1),
        mainEl = q('.content.' + backPath),
        floatingEl = q('.content.' + backPath + '-' + floating);

    mainEl.classList.remove('hidden');
    floatingEl.classList.remove('foreground');
    floatingEl.classList.remove('active');

    setTimeout(() => {
        mainEl.classList.remove('background')
        if (mainEl.classList.contains('floating')) {
            mainEl.classList.add('foreground');
        }
    }, 20);
    setTimeout(() => {
        floatingEl.classList.add('hidden')
        if (mainEl.classList.contains('floating')) {
            mainEl.classList.add('active');
        }
    }, 500);

    curUiView = backPath;
    syncBackgroundUiStateDebounce();
};

// Hides all views without any fancy CSS animations.
const resetViewState = () => {
    for (let uiView of divsUiView) {
        if (uiView.classList.contains('floating')) {
            uiView.classList.add('hidden');
            uiView.classList.remove('active', 'foreground');
        } else {
            uiView.classList.add('hidden');
            uiView.classList.remove('background');
        }
    }
    liNavCreate.classList.remove('sel');
    liNavDecrypt.classList.remove('sel');
};

// Show the Create tab view
const showCreateUiView = () => {
    resetViewState();
    q('.content.create').classList.remove('hidden');
    liNavCreate.classList.add('sel');
    curUiView = 'create';
    syncBackgroundUiStateDebounce();
};

// Show the Create Results tab view
const showCreateResultsUiView = () => {
    resetViewState();
    q('.content.create-results').classList.remove('hidden');
    q('.content.create-results').classList.add('active', 'foreground');
    liNavCreate.classList.add('sel');
    curUiView = 'create-results';
    syncBackgroundUiStateDebounce();
};

// Show the Manage Saved Text UI View
const showCreateManageUiView = () => {
    resetViewState();
    loadManageSavedTextTable();
    q('.content.create-manage').classList.remove('hidden');
    q('.content.create-manage').classList.add('active', 'foreground');
    liNavCreate.classList.add('sel');
    curUiView = 'create-manage';
    syncBackgroundUiStateDebounce();
};

// Show the Saved Text Add / Edit UI View
const showCreateManageEditUiView = () => {
    resetViewState();
    q('.content.create-manage-edit').classList.remove('hidden');
    q('.content.create-manage-edit').classList.add('active', 'foreground');
    liNavCreate.classList.add('sel');
    curUiView = 'create-manage-edit';

    if (inputHiddenSavedTextId.value)
        h1AddOrEditSavedText.textContent = i18n.createManageEditHeading;
    else
        h1AddOrEditSavedText.textContent = i18n.createManageAddHeading;

    syncBackgroundUiStateDebounce();
};

// Show the Decrypt tab view
const showDecryptUiView = () => {
    resetViewState();
    q('.content.decrypt').classList.remove('hidden');
    liNavDecrypt.classList.add('sel');
    curUiView = 'decrypt';
    syncBackgroundUiStateDebounce();
};

// Show the Decrypt Results tab view
const showDecryptResultsUiView = () => {
    resetViewState();
    q('.content.decrypt-results').classList.remove('hidden');
    q('.content.decrypt-results').classList.add('active', 'foreground');
    liNavDecrypt.classList.add('sel');
    curUiView = 'decrypt-results';
    syncBackgroundUiStateDebounce();
};

// Maximizes the Browser Action popup to a full tab
const maximizePopupToFullTab = () => {
    sendMessage('maximize-browser-action', {});
    window.close();
};

// reset the state of our Memespeech instance
const resetMemespeech = () => {
    memespeech = new Memespeech({ debugMode: false });
};

// open a tooltip
const openTooltip = async function(e) {
    let key = findTarget(e.target, 'div').dataset.tip,
        tooltip = tooltips[key];

    imgModalTooltip.src = tooltip.image;
    h3ModalTooltip.textContent = tooltip.heading;
    pModalTooltip.textContent = tooltip.description;

    divFlexLayout.classList.add('blur');
    divModalTooltip.classList.remove('hidden');
    await wait(20);
    divModalTooltip.classList.remove('fade');
};

// close the tooltip
const closeTooltip = function() {
    divFlexLayout.classList.remove('blur');
    divModalTooltip.classList.add('hidden', 'fade');
};

// show the Saved Text popup
const showSavedTextPopup = e => {
    e.preventDefault();

    sendMessage('get-saved-text-summary', {}, response => {
        let linkPos = aChooseSavedText.getBoundingClientRect(),
            windowWidth = document.documentElement.clientWidth;

        // sort the Saved Text Summary
        response.savedTextSummary.sort((a, b) => b.title.localeCompare(a.title));

        // wipe all existing elements out of the saved text popup
        while (ulSavedTextPopupItems.children.length > 1)
            ulSavedTextPopupItems.removeChild(ulSavedTextPopupItems.firstChild);

        // render the list of Saved Text options using the data from background
        for (let savedText of response.savedTextSummary) {
            let li = c('li'), a = c('a'), span = c('span');

            span.textContent = savedText.title;
            a.href = '#';
            a.dataset.id = savedText.id;
            a.addEventListener('click', handleChooseSavedText);

            a.appendChild(span);
            li.appendChild(a);
            ulSavedTextPopupItems.insertBefore(li, ulSavedTextPopupItems.childNodes[0]);
        }

        divSavedTextPopup.style.right = (windowWidth - linkPos.right) + 'px';
        divSavedTextPopup.style.top = linkPos.top + 'px';
        divSavedTextPopup.classList.remove('hidden');
    });
};

// hide the Saved Text dropdown list popup
const hideSavedTextPopup = e => divSavedTextPopup.classList.add('hidden');

// when user chooses from the popup, grab Saved Text from the background script
const handleChooseSavedText = e => {
    e.preventDefault();
    hideSavedTextPopup();
    
    let id = findTarget(e.target, 'a').dataset.id;

    sendMessage('get-saved-text', { savedTextId: id }, response => {
        if (textareaCarrier.value)
            textareaCarrier.value += ' ' + response.data.content;
        else
            textareaCarrier.value = response.data.content;

        handleTextareaCarrierChange();
        syncBackgroundUiStateDebounce();
    })
};

// open the Manage Saved Text UI View after populating its data
const handleManageSavedText = async function(e) {
    if (e) e.preventDefault();
    hideSavedTextPopup();
    loadManageSavedTextTable();
    showFloatingView('create-manage');
};

// loads the table of options in the Managed Saved Text UI View
const loadManageSavedTextTable = () => {
    sendMessage('get-saved-text-summary', {}, async function(response) {

        // wipe all existing elements out of the management table
        while (tbodyCreateManage.children.length)
            tbodyCreateManage.removeChild(tbodyCreateManage.firstChild);

        // render the list of Saved Text options using the data from background
        for (let savedText of response.savedTextSummary) {
            let tr = c('tr'), td1 = c('td'), td2 = c('td'), a = c('a');

            let svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg'),
                use = document.createElementNS('http://www.w3.org/2000/svg', 'use');

            a.href = '#';
            a.textContent = savedText.title;
            a.dataset.id = savedText.id;
            a.addEventListener('click', handleEditSavedText);
            td1.appendChild(a);

            svg.dataset.id = savedText.id;
            use.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', '#icon-delete');
            use.setAttributeNS('http://www.w3.org/1999/xlink', 'href', '#icon-delete');
            svg.addEventListener('click', handleDeleteSavedText);
            svg.appendChild(use);
            td2.appendChild(svg);

            tr.appendChild(td1);
            tr.appendChild(td2);
            tbodyCreateManage.appendChild(tr);
        }
    });
};

// opens the Add Saved Text UI View and initializes the form
const handleAddSavedText = e => {
    h1AddOrEditSavedText.textContent = i18n.createManageAddHeading;
    inputSavedTextTitle.value = '';
    textareaSavedText.value = '';
    inputHiddenSavedTextId.value = '';
    handleSavedTextButtonState();
    showFloatingView('create-manage-edit');
};

// opens the Edit Saved Text UI View after loading data for the form
const handleEditSavedText = e => {
    if (e) e.preventDefault();

    let id = findTarget(e.target, 'a').dataset.id;

    sendMessage('get-saved-text', { savedTextId: id }, response => {
        h1AddOrEditSavedText.textContent = i18n.createManageEditHeading;
        inputSavedTextTitle.value = response.data.title;
        textareaSavedText.value = response.data.content;
        inputHiddenSavedTextId.value = response.data.id;
        handleSavedTextButtonState();
        showFloatingView('create-manage-edit');
    });
};

// handles the deletion of Saved Text. There is no confirmation. Caveat emptor.
const handleDeleteSavedText = e => {
    if (e) e.preventDefault();

    let target = findTarget(e.target, 'svg'),
        id = target.dataset.id,
        tr = target.parentNode.parentNode;

    tr.parentNode.removeChild(tr);
    sendMessage('delete-saved-text', { existingId: id }, response => {
        // loadManageSavedTextTable();  // kind of redundant
    });
};

// decide whether to enable or disable the Save button for Saved Text UI View
const handleSavedTextButtonState = e => {
    if (!inputSavedTextTitle.value.length || !textareaSavedText.value.length)
        buttonSubmitSavedText.disabled = true;
    else
        buttonSubmitSavedText.disabled = false;

    // sync state with the background script if this came from a user action
    if (e) syncBackgroundUiStateDebounce();
};

// handle submit event for the Saved Text UI View
const handleSavedTextSubmit = e => {
    if (buttonSubmitSavedText.disabled) return false;

    applyLoadingState('create-manage-edit', true);

    sendMessage('do-save-text', {
        title: inputSavedTextTitle.value,
        body: textareaSavedText.value,
        existingId: inputHiddenSavedTextId.value
    }, response => {
        applyLoadingState('create-manage-edit', false);
        dismissSavedTextAddOrEdit();
    });
};

// dismisses the Saved Text Add / Edit and goes back to the Manage UI View
const dismissSavedTextAddOrEdit = () => {
    loadManageSavedTextTable();
    q('.content.create-manage').classList.add('active', 'background');
    hideFloatingView('create-manage-edit');
}

// open the credits modal
const openCredits = async function(e) {
    if (!creditsLastAnimated) {
        const _animateLogo = () => {
            const now = new Date().getTime();
            if (now - creditsLastAnimated < 150) return;

            for (const char of qs('h1.logo-text b'))
                char.classList[Math.random() > .5 ? 'add' : 'remove']('tweak');

            creditsLastAnimated = new Date().getTime();
        };
        creditsLastAnimated = new Date().getTime();
        window.addEventListener('mousemove', _animateLogo);
    }
    divFlexLayout.classList.add('blur');
    divCreditsPopup.classList.remove('hidden');
    await wait(20);
    divCreditsPopup.classList.remove('fade');
};

// close the credits modal
const closeCredits = e => {
    divFlexLayout.classList.remove('blur');
    divCreditsPopup.classList.add('hidden', 'fade');
};

// handle toggling of the config checkbox
const handleToggleConfig = e => {
    let isChecked = inputToggleConfig.checked;

    if (isChecked)
        document.body.classList.add('cool-mode');
    else
        document.body.classList.remove('cool-mode');

    if (e) {
        config.coolMode = isChecked ? true : false;
        sendMessage('set-config', { config });
    }
}

// sync the extension state to background after a brief (restartable) delay
const syncBackgroundUiStateDebounce = () => {
    if (FULLSCREEN) return;

    const _syncBackgroundUiState = () => {
        let uiState = {
            curUiView,
            textareaCarrier:            textareaCarrier.value,
            inputEnableEncryption:      inputEnableEncryption.checked,
            inputPlaintext:             inputPlaintext.value,
            inputPassword:              inputPassword.value,
            textareaCreateResults:      textareaCreateResults.value,
            textareaMemespeech:         textareaMemespeech.value,
            inputDecryptionPassword:    inputDecryptionPassword.value,
            textareaDecrypted:          textareaDecrypted.value,
            inputSavedTextTitle:        inputSavedTextTitle.value,
            textareaSavedText:          textareaSavedText.value,
            inputHiddenSavedTextId:     inputHiddenSavedTextId.value
        }
        sendMessage('sync-ui-state', { uiState });
    }
    if (backgroundSyncDebounce) clearTimeout(backgroundSyncDebounce);
    backgroundSyncDebounce = setTimeout(_syncBackgroundUiState, 550);
};

// set the UI state using data from the background script when the page loads
const initializeUiStateFromBackground = uiState => {
    if (uiState.curUiView === 'none') return;

    inputEnableEncryption.checked   = uiState.inputEnableEncryption;
    textareaCarrier.value           = uiState.textareaCarrier;
    inputPlaintext.value            = uiState.inputPlaintext;
    inputPassword.value             = uiState.inputPassword;
    textareaCreateResults.value     = uiState.textareaCreateResults;
    textareaMemespeech.value        = uiState.textareaMemespeech;
    inputDecryptionPassword.value   = uiState.inputDecryptionPassword;
    textareaDecrypted.value         = uiState.textareaDecrypted;
    inputSavedTextTitle.value       = uiState.inputSavedTextTitle;
    textareaSavedText.value         = uiState.textareaSavedText;
    inputHiddenSavedTextId.value    = uiState.inputHiddenSavedTextId;

    switch (uiState.curUiView) {
        case 'create':
            showCreateUiView();
            break;
        case 'create-results':
            showCreateResultsUiView();
            break;
        case 'create-manage':
            showCreateManageUiView();
            break;
        case 'create-manage-edit':
            showCreateManageEditUiView();
            break;
        case 'decrypt':
            showDecryptUiView();
            break;
        case 'decrypt-results':
            showDecryptResultsUiView();
            break;
    }
};

// bind events and do housekeeping, after the background script sends our data
const documentLoaded = uiState => {
    resetMemespeech();

    // set ui state from background
    initializeUiStateFromBackground(uiState);

    // nav events
    liNavCreate.addEventListener('click', showCreateUiView);
    liNavDecrypt.addEventListener('click', showDecryptUiView);
    divOpenNewTab.addEventListener('click', maximizePopupToFullTab);

    // Create view events
    handleInputEnableEncryption();
    inputEnableEncryption.addEventListener('change', handleInputEnableEncryption);
    textareaCarrier.addEventListener('change', handleTextareaCarrierChange);
    textareaCarrier.addEventListener('keyup', handleTextareaCarrierChange);
    textareaCarrier.addEventListener('paste', handleTextareaCarrierChange);
    textareaCarrier.addEventListener('cut', handleTextareaCarrierChange);
    inputPlaintext.addEventListener('change', handleByteBudget);
    inputPlaintext.addEventListener('keyup', handleByteBudget);
    buttonCreateSubmit.addEventListener('click', handleCreateSubmit);
    buttonCreateReset.addEventListener('click', handleCreateReset);
    buttonCreateRegenerate.addEventListener('click', handleCreateRegenerate);
    inputPassword.addEventListener('keyup', checkCreatePasswordEnterKey);
    divCreateResultsBack.addEventListener('click', () => hideFloatingView('create'));
    svgIconCopy.addEventListener('click', copyMemespeechToClipboard);
    handleTextareaCarrierChange();

    // Decrypt view events
    textareaMemespeech.addEventListener('change', handleDecryptButtonState);
    textareaMemespeech.addEventListener('keyup', handleDecryptButtonState);
    textareaMemespeech.addEventListener('paste', handleDecryptButtonState);
    textareaMemespeech.addEventListener('cut', handleDecryptButtonState);
    buttonDecryptSubmit.addEventListener('click', handleDecryptSubmit);
    buttonDecryptClose.addEventListener('click', handleDecryptResultsClose);
    inputDecryptionPassword.addEventListener('keyup', checkDecryptPasswordEnterKey);
    divDecryptResultsBack.addEventListener('click', () => handleDecryptResultsClose(true));
    handleDecryptButtonState();

    // Tooltip view events
    for (let tip of qs('.content div.tooltip')) tip.addEventListener('click', openTooltip);
    buttonModalTooltipClose.addEventListener('click', closeTooltip);

    // Saved Text events
    aChooseSavedText.addEventListener('click', showSavedTextPopup);
    divSavedTextPopup.addEventListener('mouseleave', hideSavedTextPopup);
    aManageSavedText.addEventListener('click', handleManageSavedText);
    divCreateManageBack.addEventListener('click', () => hideFloatingView('create-manage'));
    buttonCreateManageDone.addEventListener('click', () => hideFloatingView('create-manage'));
    divCreateManageAdd.addEventListener('click', handleAddSavedText);
    divCreateManageEditBack.addEventListener('click', dismissSavedTextAddOrEdit);
    textareaSavedText.addEventListener('change', handleSavedTextButtonState);
    textareaSavedText.addEventListener('keyup', handleSavedTextButtonState);
    textareaSavedText.addEventListener('paste', handleSavedTextButtonState);
    textareaSavedText.addEventListener('cut', handleSavedTextButtonState);
    inputSavedTextTitle.addEventListener('change', handleSavedTextButtonState);
    buttonSubmitSavedText.addEventListener('click', handleSavedTextSubmit);
    handleSavedTextButtonState();

    // Credits popup events
    svgLogo.addEventListener('click', openCredits);
    buttonCreditsDismiss.addEventListener('click', closeCredits);
    inputToggleConfig.addEventListener('change', handleToggleConfig);
};

// get data from the background script and localize all HTML elements on the page
sendMessage('get-i18n-ui-state', { keys: [
    'extensionName',
    'buttonCreate',
    'buttonDecrypt',
    'buttonOpenNewTab',
    'createHeading',
    'createCarrierTextLabel',
    'createChooseSavedTextLabel',
    'createEnableEncryptionLabel',
    'createSecretTextPlaceholder',
    'createEncryptionPasswordPlaceholder',
    'createSubmitButtonText',
    'createResetButtonText',
    'createNotificationDefault',
    'createNotificationTooShort',
    'createNotificationOK',
    'createNotificationRemaining',
    'createNotificationSecretTextOverflow',
    'createNotificationMaxed',
    'createManageSavedText',
    'createManageHeading',
    'createManageDoneButtonText',
    'createManageAddHeading',
    'createManageEditHeading',
    'createManageTitleLabel',
    'createManageSavedTextLabel',
    'createManageSaveButtonText',
    'byteSingular',
    'bytePlural',
    'kilobyteSingular',
    'kilobytePlural',
    'copy',
    'copied',
    'add',
    'edit',
    'delete',
    'back',
    'createErrorPasswordIsRequired',
    'createResultsHeading',
    'createResultsMemespeechLabel',
    'createResultsRegenerateButtonText',
    'decryptHeading',
    'decryptMemespeechTextLabel',
    'decryptPasswordLabel',
    'decryptPasswordPlaceholder',
    'decryptSubmitButtonText',
    'decryptErrorCannotParse',
    'decryptErrorMissingPassphrase',
    'decryptFail',
    'decryptResultsHeading',
    'decryptResultsCloseButtonText',
    'tooltipCarrierTextHeading',
    'tooltipCarrierTextDescription',
    'tooltipEncryptionHeading',
    'tooltipEncryptionDescription',
    'tooltipMemespeechTextHeading',
    'tooltipMemespeechTextDescription',
    'tooltipDecryptionPasswordHeading',
    'tooltipDecryptionPasswordDescription',
    'tooltipDismissButtonText',
    'tooltipTitle',
    'creditsThankYouForUsing',
    'creditsEnableCoolMode'
]}, response => {
    i18n = response.vals;
    config = response.config;

    id('i18n-buttonCreate').textContent = i18n.buttonCreate;
    id('i18n-buttonDecrypt').textContent = i18n.buttonDecrypt;
    id('i18n-createHeading').textContent = i18n.createHeading;
    id('i18n-createCarrierTextLabel').textContent = i18n.createCarrierTextLabel;
    id('i18n-createEnableEncryptionLabel').textContent = i18n.createEnableEncryptionLabel;
    id('i18n-createResultsHeading').textContent = i18n.createResultsHeading;
    id('i18n-createResultsMemespeechLabel').textContent = i18n.createResultsMemespeechLabel;
    id('i18n-decryptHeading').textContent = i18n.decryptHeading;
    id('i18n-decryptMemespeechTextLabel').textContent = i18n.decryptMemespeechTextLabel;
    id('i18n-decryptPasswordLabel').textContent = i18n.decryptPasswordLabel;
    id('i18n-decryptResultsHeading').textContent = i18n.decryptResultsHeading;
    id('i18n-createManageSavedText').textContent = i18n.createManageSavedText;
    id('i18n-createManageHeading').textContent = i18n.createManageHeading;
    id('i18n-savedTextTitleLabel').textContent = i18n.createManageTitleLabel;
    id('i18n-savedTextBodyLabel').textContent = i18n.createManageSavedTextLabel;
    id('i18n-creditsThankYouForUsing').textContent = i18n.creditsThankYouForUsing;
    id('i18n-creditsEnableCoolMode').textContent = i18n.creditsEnableCoolMode;

    tooltips.carrierText.heading = i18n.tooltipCarrierTextHeading;
    tooltips.carrierText.description = i18n.tooltipCarrierTextDescription;
    tooltips.encryption.heading = i18n.tooltipEncryptionHeading;
    tooltips.encryption.description = i18n.tooltipEncryptionDescription;
    tooltips.memespeechText.heading = i18n.tooltipMemespeechTextHeading;
    tooltips.memespeechText.description = i18n.tooltipMemespeechTextDescription;
    tooltips.decryptPassphrase.heading = i18n.tooltipDecryptionPasswordHeading;
    tooltips.decryptPassphrase.description = i18n.tooltipDecryptionPasswordDescription;

    id('icon-info').querySelector('title').textContent = i18n.tooltipTitle;
    id('icon-add').querySelector('title').textContent = i18n.add;
    id('icon-delete').querySelector('title').textContent = i18n.delete;
    id('icon-arrow-left').querySelector('title').textContent = i18n.back;
    svgIconCopy.querySelector('title').textContent = i18n.copy;

    aChooseSavedText.textContent = i18n.createChooseSavedTextLabel;
    divOpenNewTab.title = i18n.buttonOpenNewTab;
    inputPlaintext.placeholder = i18n.createSecretTextPlaceholder;
    inputPassword.placeholder = i18n.createEncryptionPasswordPlaceholder;
    buttonCreateSubmit.textContent = i18n.createSubmitButtonText;
    buttonCreateReset.textContent = i18n.createResetButtonText;
    buttonCreateRegenerate.textContent = i18n.createResultsRegenerateButtonText;
    inputDecryptionPassword.placeholder = i18n.decryptPasswordPlaceholder;
    buttonDecryptSubmit.textContent = i18n.decryptSubmitButtonText;
    buttonDecryptClose.textContent = i18n.decryptResultsCloseButtonText;
    buttonModalTooltipClose.textContent = i18n.tooltipDismissButtonText;
    spanCopyNotice.textContent = i18n.copied;
    buttonCreateManageDone.textContent = i18n.createManageDoneButtonText;
    buttonSubmitSavedText.textContent = i18n.createManageSaveButtonText;
    buttonCreditsDismiss.textContent = i18n.tooltipDismissButtonText;

    inputToggleConfig.checked = config.coolMode ? true : false;
    handleToggleConfig();
    document.title = i18n.extensionName;

    documentLoaded(response.uiState);
});

/*
NOTE: Chrome has an annoying bug where the Browser Action popup is limited to
600px height, which is fine, except they don't scale this value when the user
overrides the zoom level of the browser. So any body height value > 480px will
result in a vertical scrollbar when Chrome's browser zoom is set at 125%.

We therefore reserve .big-boi with its larger non-scaled height for Firefox.
*/
if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
    document.body.classList.add('big-boi');
}

/*
NOTE: Chrome has another annoying bug in recent versions where the height of the
layout's flex container is miscomputed initially. THIS WILL FIX IT.
*/
divFlexLayout.style.height = '100vh';