'use strict';

const animate = () => {
    const ANIMATION_TIME    = 520,  // milliseconds
          CENTER            = 250,  // pixels
          FACE_ROTATION     = 20,   // degrees
          EYE_END_RADIUS    = 20,
          EYE_X_OFFSET      = 60,
          EYE_Y_OFFSET      = 30,
          MOUTH_END_RADIUS  = 40,
          MOUTH_X_OFFSET    = 45,
          MOUTH_Y_OFFSET    = 30,
          START_TIME        = new Date().getTime();

    let eyeLeftX    = CENTER - EYE_X_OFFSET,
        eyeRightX   = CENTER + (EYE_X_OFFSET - 2 * EYE_END_RADIUS),
        eyeY        = CENTER - EYE_Y_OFFSET,
        mouthStartX = CENTER - MOUTH_X_OFFSET,
        mouthEndX   = CENTER + MOUTH_X_OFFSET,
        mouthY      = CENTER + MOUTH_Y_OFFSET;

    let back        = document.body,
        symbol      = q('svg.symbol'),  // q is short for document.querySelector
        face        = q('svg.smiley'),  // see common.js
        faceBg      = q('svg.smiley .face'),
        eyeLeft     = q('path.eye.left'),
        eyeRight    = q('path.eye.right'),
        mouth       = q('path.mouth');

    const animateFace = () => {
        requestAnimationFrame(() => {
            var curTime     = new Date().getTime(),
                pct         = (curTime - START_TIME) / ANIMATION_TIME,
                pct         = pct > 1 ? 1 : pct,
                eyeWidth    = (2 - (pct / pct)) * EYE_END_RADIUS, // ease in
                eyeRadius   = pct * EYE_END_RADIUS,
                mouthRadius = pct * MOUTH_END_RADIUS,
                mouthCurve  = (10 - (pct*10)),
                mouthRadius  = mouthRadius < mouthCurve ? mouthCurve : mouthRadius,
                rotate      = (1 - pct) * FACE_ROTATION;

            eyeLeft.setAttribute('d', 'M ' + eyeLeftX + ' ' + eyeY + ' '
                + 'A ' + eyeWidth + ' ' + eyeRadius + ' 0 0 1 '
                + (eyeLeftX + 2*EYE_END_RADIUS) + ' ' + eyeY + ' A '
                + eyeWidth + ' ' + eyeRadius + ' 0 0 1 '
                + eyeLeftX + ' ' + eyeY);

            eyeRight.setAttribute('d', 'M ' + eyeRightX + ' ' + eyeY + ' '
                + 'A ' + eyeWidth + ' ' + eyeRadius + ' 0 0 1 '
                + (eyeRightX + 2*EYE_END_RADIUS) + ' ' + eyeY + ' A '
                + eyeWidth + ' ' + eyeRadius + ' 0 0 1 '
                + eyeRightX + ' ' + eyeY);

            mouth.setAttribute('d', 'M ' + mouthStartX + ' ' + mouthY + ' A '
                + MOUTH_X_OFFSET + ' ' + mouthCurve + ' 0 0 0 ' + mouthEndX + ' ' + mouthY + ' '
                + 'A ' + MOUTH_X_OFFSET + ' ' + mouthRadius + ' 0 0 1 '
                + mouthStartX + ' ' + mouthY);

            face.style.transform = 'translateY(-50%) '
                                 + 'scale(' + pct + ') '
                                 + 'rotate( ' + rotate + 'deg)';

            if (pct < 1) animateFace();
            else {
                back.classList.add('happy');
                setTimeout(() => {
                    back.classList.add('content-visible');
                    window.addEventListener('mousemove', animateLogo);
                }, 500);
            }
        });
    };
    animateFace();
}

let logoLastChanged = null;

const animateLogo = () => {
    const now = new Date().getTime();
    
    if (logoLastChanged && now - logoLastChanged < 150) return;

    for (const letter of qs('h1.logo-text b'))
        letter.classList[Math.random() > .5 ? 'add' : 'remove']('tweak');

    logoLastChanged = new Date().getTime();
}

sendMessage('get-i18n-ui-state', { keys: [
    'splashPrivacyAndTermsHeading',
    'splashDataStorageDisclosure',
    'splashLicenseTermSentenceBeginning',
    'splashLicenseTermSentenceLicenseDescription',
    'splashLicenseTermSentenceSourceCodeLinkText',
    'splashEncryptionWarning',
    'splashButtonText',
    'splashWindowTitle'
]}, (message) => {
    let vals = message.vals;
    id('i18n-splashPrivacyAndTermsHeading').textContent = vals.splashPrivacyAndTermsHeading;
    id('i18n-splashDataStorageDisclosure').textContent = '\u2022 ' + vals.splashDataStorageDisclosure;
    id('i18n-splashLicenseTermSentenceBeginning').textContent = '\u2022 ' + vals.splashLicenseTermSentenceBeginning + ' ';
    id('i18n-splashLicenseTermSentenceLicenseDescription').textContent = vals.splashLicenseTermSentenceLicenseDescription;
    id('i18n-splashLicenseTermSentenceSourceCodeLinkText').textContent = vals.splashLicenseTermSentenceSourceCodeLinkText;
    id('i18n-splashEncryptionWarning').textContent = '\u2022 ' + vals.splashEncryptionWarning;
    id('i18n-splashButtonText').textContent = vals.splashButtonText;
    document.title = vals.splashWindowTitle;

    setTimeout(animate, 900);
    q('button').addEventListener('click', () => sendMessage('close-splash-screen', {}));
});