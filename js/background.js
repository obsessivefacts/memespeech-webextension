'use strict';

/*
Handle the context menu items.
*/
chrome.contextMenus.create({
    id: 'convert-to-memespeech',
    title: chrome.i18n.getMessage('menuItemSelectionConvertToMemespeech'),
    contexts: ['selection']
});

chrome.contextMenus.create({
    id: 'decrypt-memespeech',
    title: chrome.i18n.getMessage('menuItemSelectionDecryptMemespeech'),
    contexts: ['selection']
});

chrome.contextMenus.onClicked.addListener(info => {
    switch (info.menuItemId) {
        case 'convert-to-memespeech':
            uiState = {
                ...uiState,
                curUiView: 'create',
                textareaCarrier: info.selectionText,
                inputEnableEncryption: false,
                inputPlaintext: '',
                inputPassword: ''
            };
            break;

        case 'decrypt-memespeech':
            uiState = {
                ...uiState,
                curUiView: 'decrypt',
                textareaMemespeech: info.selectionText,
                inputDecryptionPassword: ''
            };
            break;
    }

    if (typeof chrome.browserAction.openPopup === 'function')
        chrome.browserAction.openPopup();
    else
        chrome.tabs.create({active: true, url: '/html/popup.html#FULLSCREEN'});
});

/*
Handle the config storage.
*/
let config = {};

const setConfig = (configObj, callback) => {
    config = configObj;
    chrome.storage.local.set({ config }, callback);
};

/*
Enable Splash Screen functionality
*/
let splashTab;

const showSplashScreen = () => {
    chrome.tabs.create({active: true, url: '/html/splash.html'}, tab => {
        splashTab = tab;
        chrome.storage.local.set({splashScreenShown: true});
    });
};

/*
Allow initialization of Saved Text data storage on install
*/
const initializeSavedText = () => {
    let txt1        = chrome.i18n.getMessage("savedTextBillOfRights"),
        txt1Title   = chrome.i18n.getMessage("savedTextBillOfRightsTitle"),
        txt2        = chrome.i18n.getMessage("savedTextZuckerberg"),
        txt2Title   = chrome.i18n.getMessage("savedTextZuckerbergTitle"),
        txt3        = chrome.i18n.getMessage("savedTextREEEEEE"),
        txt3Title   = chrome.i18n.getMessage("savedTextREEEEEETitle"),
        now         = new Date().getTime();

    chrome.storage.local.set({
        'savedTextSummary': [
            { id: 1, title: txt1Title, created: now, length: txt1.length },
            { id: 2, title: txt2Title, created: now, length: txt2.length },
            { id: 3, title: txt3Title, created: now, length: txt3.length }
        ],
        'savedText-1': txt1,
        'savedText-2': txt2,
        'savedText-3': txt3
    });
};

/*
Handle Saved Text submission from the UI
*/
const doSaveText = (payload, callback) => {
    chrome.storage.local.get(['savedTextSummary'], result => {
        let savedTextSummary = result.savedTextSummary,
            now = new Date().getTime(),
            id = payload.existingId ? parseInt(payload.existingId) : now,
            existIndex = savedTextSummary.findIndex(saved => saved.id === id);

        if (existIndex > -1) {
            savedTextSummary[existIndex].title = payload.title;
            savedTextSummary[existIndex].length = payload.body.length;
        } else {
            savedTextSummary.push({
                id,
                created: now,
                title: payload.title,
                length: payload.body.length
            });
        }
        savedTextSummary.sort((a, b) => a.title.localeCompare(b.title));

        chrome.storage.local.set({
            savedTextSummary,
            ['savedText-'+id]: payload.body
        }, callback);
    });
};

/*
Delete a Saved Text record at the request of the UI
*/
const doDeleteSavedText = (payload, callback) => {
    chrome.storage.local.get(['savedTextSummary'], result => {
        let savedTextSummary = result.savedTextSummary.filter(savedText => 
            savedText.id !== parseInt(payload.existingId));

        chrome.storage.local.remove('savedText-' + payload.existingId, () => {
            chrome.storage.local.set({ savedTextSummary }, callback);
        });
    });
};

/*
Manage the UI state so we can preserve the state of the Browser Action when the
user closes it, as well as allow expanding the Browser Action to a full tab. The
UI state may contain private information, so we will clear this out after 5 min.
*/
let uiState,
    stateResetTimeout = null,
    clearStateOnNextLoad = false;

const resetUiState = () => uiState = {
    curUiView: 'none',
    textareaCarrier:            null,
    inputEnableEncryption:      null,
    inputPlaintext:             null,
    inputPassword:              null,
    textareaCreateResults:      null,
    textareaMemespeech:         null,
    inputDecryptionPassword:    null,
    textareaDecrypted:          null,
    inputSavedTextTitle:        null,
    textareaSavedText:          null,
    inputHiddenSavedTextId:     null
};
resetUiState();

/*
Show the splash screen and initialize our Saved Text if it's a new installation.
Also loads (or initializes) the config into a persistent variable.
*/
chrome.storage.local.get(['splashScreenShown', 'savedTextSummary', 'config'],
    result => {
        if (!result.splashScreenShown) showSplashScreen();
        if (!result.savedTextSummary) initializeSavedText();
        if (!result.config) setConfig({});
        else config = result.config;
    });

/*
Listen for messages and send responses
*/
chrome.runtime.onConnect.addListener(port => {
    port.onMessage.addListener(message => {
        switch (message.type) {

            // close the splash screen when the user clicks the OK button
            case 'close-splash-screen':
                if (splashTab) chrome.tabs.remove(splashTab.id);
                break;

            // allow syncing of UI state from Browser Action
            // ... but clear it out after awhile
            case 'sync-ui-state':
                uiState = message.uiState;
                if (stateResetTimeout) clearTimeout(stateResetTimeout);
                stateResetTimeout = setTimeout(resetUiState, 300000);   // 5 min
                break;

            // deliver our localized text and synced UI state to the page
            case 'get-i18n-ui-state':
                let vals = {};
                for (let k of message.keys) vals[k] = chrome.i18n.getMessage(k);
                port.postMessage({id: message.id, vals, uiState, config});
                if (clearStateOnNextLoad) {
                    clearStateOnNextLoad = false;
                    resetUiState();
                }
                break;

            // allow maximizing the browser action to a full tab
            case 'maximize-browser-action':
                clearStateOnNextLoad = true;
                chrome.tabs.create({
                    active: true,
                    url: '/html/popup.html#FULLSCREEN'
                });
                break;

            // get the Saved Text summary from local storage
            case 'get-saved-text-summary':
                chrome.storage.local.get(['savedTextSummary'], result =>
                    port.postMessage({ id: message.id, ...result }));
                break;

            // get an individual Saved Text item
            case 'get-saved-text':
                chrome.storage.local.get([
                    'savedTextSummary',
                    'savedText-' + message.savedTextId
                ], result => {
                    let data = result.savedTextSummary.find(el => {
                        return el.id == message.savedTextId
                    });
                    data.content = result['savedText-' + message.savedTextId];
                    port.postMessage({ id: message.id, data });
                });
                break;

            // handle Saved Text submission from UI (create or update)
            case 'do-save-text':
                doSaveText(message, () => port.postMessage({id: message.id}));
                break;

            // delete a Saved Text entry
            case 'delete-saved-text':
                doDeleteSavedText(message, () => port.postMessage({
                    id: message.id, kthxbai: true
                }));
                break;

            // set our configuration
            case 'set-config':
                setConfig(message.config);
                break;
        }
    });
});
